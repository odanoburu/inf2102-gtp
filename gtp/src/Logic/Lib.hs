{-|
Module      : Logic.Lib
Description : Library of logic functions
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Library module for logic functions. Mostly export useful functions
from other modules.
-}
module Logic.Lib (Logic(..), onFormula) where

import qualified Logic.MIMP as M
import qualified Logic.CP as C
import Logic.Embed (embedClassicalMinimalImplicational)

-- | Datatype for the logics supported by the system.
data Logic = Classical
--- TODO:
  --  | Intuitionistic
  --  | Minimal
  | MinimalImplicational
  deriving (Eq, Read, Show)

onFormula :: Logic -> String -> (Either String M.Formula -> b) -> b
-- | @'onFormula' logic formula f@: parse concrete syntax of @formula@
-- of @logic@ and apply @f@ to the result.
onFormula logic formulaStr f = f $ go logic
  where
    go MinimalImplicational = M.parse formulaStr
    go Classical = embedClassicalMinimalImplicational
      <$> C.parse formulaStr
