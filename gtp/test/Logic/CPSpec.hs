module Logic.CPSpec (spec) where

import Test.Hspec

import Logic.CP (Formula'(..), parse)


spec :: Spec
spec =
  describe "formula parsing" $ do
    it "parses any group of alphanumeric characters as an atom" $
      parse "A1" `shouldBe` (Right $ Atom "A1")

    it "parses parenthesized prefixed implications" $
      parse "(-> A1 A1)" `shouldBe` (Right $ Impl (Atom "A1") (Atom "A1"))

    it "parses parenthesized prefixed and" $
      parse "(& A1 A1)" `shouldBe` (Right $ And (Atom "A1") (Atom "A1"))

    it "parses parenthesized prefixed or" $
      parse "(| A1 A1)" `shouldBe` (Right $ Or (Atom "A1") (Atom "A1"))

    it "parses parenthesized prefixed not" $
      parse "(~ (| A1 A1))" `shouldBe` (Right $ Not (Or (Atom "A1") (Atom "A1")))
