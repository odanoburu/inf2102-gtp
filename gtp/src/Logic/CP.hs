{-|
Module      : Logic.CP
Description : Classical logic definitions
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Defines a data type for classical propositional (CP) formulas and a
parser for their concrete syntax.
-}
{-# LANGUAGE DeriveFunctor #-}
module Logic.CP (Formula'(..), Formula, isPositive, parse) where

import Logic.Parser (Parser, NodeLabel, atomLabel, parens, symbol, parseLogic)

import Text.Megaparsec hiding (parse)


-- | Datatype for CP formulas.
data Formula' a
  = Atom a
  | Impl {left :: Formula' a, right :: Formula' a}
  | And {left :: Formula' a, right :: Formula' a}
  | Or {left :: Formula' a, right :: Formula' a}
  | Not (Formula' a)
  deriving (Eq, Functor, Ord)

instance Show a => Show (Formula' a) where
  show (Atom x) = show x
  show (Not x) = '¬' : show x
  show f = concat ["(", op, " ", show.left $ f, " ", show.right $ f, ")"]
    where
      op = case f of
        (Impl _ _) -> "->"
        (And _ _)  -> "∧"
        (Or _ _)   -> "∨"
        (Not _)  -> undefined
        (Atom _) -> undefined


isPositive :: Formula' a -> Bool
-- | Predicate for positive formulas (those that do not contain the
-- 'Not' connective)
isPositive Atom{} = True
isPositive Not{} = False
isPositive f = isPositive (left f) && isPositive (right f)

type Formula = Formula' NodeLabel

prefixedFormula :: Parser Formula
prefixedFormula
  = parens (binary <|> (Not <$ symbol "~" <*> prefixedFormula))
  <|>(Atom <$> atomLabel)
  where
    binaryOp = Impl <$ symbol "->" <|> And <$ symbol "&" <|> Or <$ symbol "|"
    binary = binaryOp <*> prefixedFormula <*> prefixedFormula


parse :: String -> Either String Formula
-- | parse CP formulas
parse = parseLogic prefixedFormula

