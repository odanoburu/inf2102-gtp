module Graph.BoltSpec (spec, getConfig) where

import Test.Hspec

import Graph.Bolt ( GoalId(..), RootId(..),
                    bolt, clearGraph, deleteProof, loadFormula, showRoots,
                    testBoltConnection
                  )
import Graph.Lib (Config, getDefaultConfigDir, readConfig)
import Logic.MIMP (Formula'(..), fibonacci)

import Control.Monad.Reader (runReaderT)
import Database.Bolt (BoltActionT)

getConfig :: IO Config
getConfig = getDefaultConfigDir >>= readConfig

go :: BoltActionT IO a -> IO a
go ac = getConfig >>= runReaderT (bolt ac)

spec :: Spec
spec =
  beforeAll (getConfig >>= runReaderT testBoltConnection)
    (describe "BOLT interface" $ do
      it "loads atomic formulas"
        $ go (loadFormula (Atom "A1"))
        `shouldNotReturn` (RootId $ -1, GoalId $ -1)

      it "loads non-atomic formulas"
        $ go (loadFormula $ fibonacci 3) `shouldNotReturn` (RootId $ -1, GoalId $ -1)

      it "deletes loaded proofs"
        $ (getConfig >>= runReaderT (bolt (loadFormula $ fibonacci 4) >>= (\(rootId, _) -> deleteProof rootId)))
        `shouldReturn` True

      it "shows loaded roots"
        $ (getConfig >>= runReaderT (do
        (rootId, _) <- bolt $ loadFormula $ fibonacci 3
        rootIds <- map fst <$> showRoots
        clearGraph
        return $ rootId `elem` rootIds)) `shouldReturn` True)
