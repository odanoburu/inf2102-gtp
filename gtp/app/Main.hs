{-|
Module      : Main
Description : gtp's CLI
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declares a CLI for gtp, which is mainly meant for administration. Most
users will want to use the web interface for creating proofs. After
installing gtp, @gtp --help@ will show an overview of the available
commands.
-}
module Main (main) where

import Graph.ATP (solveFormula, solveGoal, solveRoot)
import Graph.Bolt ( GoalId(..), RootId, bolt, clearGraph, graphStats, loadFormula
                  , testBoltConnection )
import Graph.Lib (logMsg, runApp)
import Graph.Reader (parseGraph, economicalHamiltonian)
import Graph.Server (LogLevel(..), gtpServe)
import Logic.Lib (Logic(..), onFormula)
import Logic.Embed (embedClassicalMinimalImplicational)


import Control.Monad.IO.Class (liftIO)
import Data.Aeson.Encode.Pretty (encodePrettyToTextBuilder)
import Data.Text.Lazy.Builder (toLazyText)
import qualified Data.Text.Lazy.IO as TIO
import Network.Wai.Handler.Warp (Port)
import Options.Applicative


data GTPCommand = GTPCommand (Maybe FilePath) GTPsub
  deriving (Eq, Show)

data GTPsub
  = Serve Port LogLevel
  | Solve Int ToSolve
  | Load Input String
  | Clear
  | Stats
  | Hamiltonian String
  deriving (Eq, Show)

data Input = Input Logic InputKind deriving (Eq, Show)
data InputKind = File | Formula deriving (Eq, Show)
data ToSolve = Unloaded Input String | Goal GoalId | Root RootId deriving (Eq,Show)

showHelpOnErrorExecParser :: ParserInfo a -> IO a
showHelpOnErrorExecParser = customExecParser (prefs showHelpOnError)


parseSubCommand :: Parser GTPsub -> Parser GTPCommand
parseSubCommand subcommand = GTPCommand <$> configDir <*> subcommand
  where
    configDir = option (Just <$> str)
      (metavar "CONFIGDIR" <> short 'c' <> long "config-dir"
       <> help "Directory where configuration files are in"
       <> value Nothing)


subcommands :: (Parser a -> Parser b) -> [(String, Parser a, String)] -> Parser b
subcommands f = hsubparser . foldMap raise
  where
    raise (name, p, desc) = command name (info (f p) (fullDesc <> progDesc desc))

parseCommand :: Parser GTPCommand
parseCommand = subcommands parseSubCommand
  [ ("serve", serveP, "Start proof server (make a request for docs/ to see documentation)")
  , ("load", loadP, "Load formula into graph store")
  , ("solve", solveP, "Try to automatically solve a formula")
  , ("clear", pure Clear, "Clear stored graph. All data will be lost")
  , ("stats", pure Stats, "Print statistics about stored graph")
  , ("hamiltonian", hamP, "Produce formula negating the existence of a hamiltonian path in GRAPH")]
  where
    serveP = Serve <$> portP <*> logLevelP
      where
        portP = option auto (metavar "PORT" <> short 'p' <> long "port"
                             <> help "Port to use" <> value 2487 <> showDefault)
        logLevelP = flag None Dev (long "log-devel" <> help "Log requests at development level" <> showDefault)
          <|> flag None Prod (long "log-prod" <> help "Log requests at production level" <> showDefault)
    loadP = Load <$> inputP <*> strArgument (metavar "INPUT")
    hamP = Hamiltonian <$> strArgument (metavar "GRAPH"
                                        <> help "Path to graph file")

inputP :: Parser Input
inputP = Input
  <$> option auto (long "logic"
                    <> short 'L'
                    <> value MinimalImplicational
                    <> help "Logic to use for INPUT (will embed more expressive logics into the minimal implicational fragment)")
  <*> (file <|> formula)
  where
    file = flag' File (short 'f' <> long "file"
                       <> help "Take INPUT to be filename of where input formula is")
    formula = flag Formula Formula (short 'F' <> long "formula"
                                    <> help "Take INPUT to be a formula")

solveP :: Parser GTPsub
solveP = subcommands (\p -> Solve <$> gasP <*> p)
  [ ("goal", goalP, "Try to prove GOAL")
  , ("root", rootP, "Try to prove all goals belonging to ROOT")
  , ("formula", formulaP, "Prove INPUT")
  ]
  where
    gasP = option auto (metavar "LIMIT" <> short 'l' <> long "limit"
                         <> help "How many iterations to make before giving up on proof" <> value 15 <> showDefault)
    goalP = Goal <$> argument auto (metavar "GOAL" <> help "Integer ID of goal to prove")
    rootP = Root <$> argument auto (metavar "ROOT" <> help "Integer ID of root node whose goals we are to prove")
    formulaP = Unloaded <$> inputP <*> strArgument (metavar "INPUT")


gtpProgDesc :: String
gtpProgDesc =
  "Show and manipulate huge proofs"

gtpHeader :: String
gtpHeader = "gtp — graph theorem prover."

main :: IO ()
main = do
  (GTPCommand mConfigDir subcommand) <- showHelpOnErrorExecParser
    $ info (helper <*> parseCommand)
    (fullDesc <> progDesc gtpProgDesc <> header gtpHeader)
  runApp mConfigDir
    (case subcommand of
       Hamiltonian graphFile -> do
         grOrErr <- liftIO $ parseGraph <$> readFile graphFile
         liftIO . putStrLn $ case grOrErr of
           Right gr -> show . embedClassicalMinimalImplicational
                       $ economicalHamiltonian gr
           Left err -> err
       _ -> testBoltConnection *> case subcommand of
              Serve port logLevel
                -> gtpServe logLevel port
              Load what input -> testBoltConnection *> load what input
                                 >>= liftIO . printLoad
              Solve gas what -> solve gas what >>= liftIO . print
              Clear -> clearGraph
              Stats -> graphStats >>= liftIO . TIO.putStrLn . toLazyText . encodePrettyToTextBuilder
              _ -> error "Internal error: CLI fall-through")
    where
      load (Input logic File) fp = liftIO (readFile fp)
        >>= load (Input logic Formula)
      load (Input logic Formula) rootFormula
        = onFormula logic rootFormula (either fail $ bolt . loadFormula)
      printLoad :: (RootId, GoalId) -> IO ()
      printLoad (rootId, goalId) = logMsg $ unwords ["Root node has ID:", show rootId, "\nGoal node has ID:", show goalId]
      solve gas (Root rootId) = solveRoot gas rootId
      solve gas (Goal goalId) = solveGoal gas goalId
      solve gas (Unloaded (Input logic Formula) rootFormula)
        = onFormula logic rootFormula (either fail $ solveFormula gas)
      solve gas (Unloaded (Input logic File) formulaFile)
        = liftIO (readFile formulaFile)
        >>= solve gas . Unloaded (Input logic Formula)
