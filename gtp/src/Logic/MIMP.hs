{-|
Module      : Logic.MIMP
Description : Minimal-implicational logic definitions
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Defines a data type for minimal-implicational (MIMP) propositional
formulas and a parser for their concrete syntax. There is also a
generator of fibonacci formulas.
-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StrictData #-}

module Logic.MIMP (Formula'(..), Formula, fibonacci, parse) where


import Logic.Parser (Parser, atomLabel, parens, parseLogic, symbol)


import Data.Aeson (FromJSON(..), ToJSON(..), withText)
import qualified Data.Text as T
import Text.Megaparsec hiding (some, token, label, parse)


-- | Datatype for MIMP formulas.
data Formula' a
  = Atom a
  | Impl {left :: Formula' a, right :: Formula' a}
  deriving (Eq, Ord)

instance {-# OVERLAPPING #-} Show (Formula' String) where
  show (Atom x) = x
  show (Impl left right) = concat ["(-> ", show left, " ", show right, ")"]

instance Show a => Show (Formula' a) where
  show (Atom x) = show x
  show (Impl left right) = concat ["(-> ", show left, " ", show right, ")"]

instance Semigroup (Formula' a) where
  f' <> f = Impl f' f

instance (Show a, ToJSON a) => ToJSON (Formula' a) where
  toJSON = toJSON . show

instance FromJSON Formula where
  parseJSON = withText "Formula" go
    where
      go formulaStr = case parse $ T.unpack formulaStr of
        Right form -> return form
        Left str -> fail str


type NodeLabel = String
-- IDEA: when we need to generalize Formula (and node!) types, use
-- typeclasses
type Formula = Formula' NodeLabel


---
-- generators
fibonacci :: Int -> Formula
-- | @'fibonacci' n@: generate fibonacci formula of degree @n@
fibonacci n
  = Impl (Impl (toAtom 1) (toAtom 2))
  $ go (Impl (toAtom 1) (toAtom n))
  where
    go acc = foldr (\i a -> Impl (Impl (toAtom $ i-2) (Impl (toAtom $ i-1)
                                                      (toAtom i))) a) acc [3..n]
    toAtom = Atom . show


---
-- parser
infixedFormula :: Parser Formula
infixedFormula = parens implication <|> (Atom <$> atomLabel)
  where
    implication
      = Impl
      <$> (infixedFormula <* symbol "->")
      <*> infixedFormula

prefixedFormula :: Parser Formula
prefixedFormula = parens implication <|> (Atom <$> atomLabel)
  where
    implication
      = Impl
      <$> (symbol "->" *> prefixedFormula)
      <*> prefixedFormula


parse :: String -> Either String Formula
-- | parse MIMP formulas
parse = parseLogic (try infixedFormula <|> prefixedFormula)

