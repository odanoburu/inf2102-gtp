{-|
Module      : Logic.Embed
Description : Embedding logics into other logics
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

This module defines embedding from logics into other logics. Currently
it is mainly meant for embedding classical propositional logic into
minimimal implicational logic.
-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
module Logic.Embed (
  -- * Embedding logics

  -- | To embed more expressive logics in less expressive ones we
  -- simulate their constructs in the less expressive logic. For
  -- propositional logics this means creating virtual
  -- propositions. One of these virtual propositions behaves as ⊥, the
  -- others simulate connectives that are not available in the less
  -- expressive logic. 'HasBottom' and 'Next' define how we can create
  -- these virtual nodes.
  HasBottom(..), Next(..), embedClassicalMinimalImplicational, true, false) where

import qualified Logic.MIMP as M
import qualified Logic.CP as C
import Logic.CP (Formula'(..))

import Data.Maybe (fromMaybe)
import qualified Data.Map.Strict as M


true, false :: HasBottom a => Formula' a
-- should be Inhabited, not HasBottom, but I'm too lazy to write more
-- boilerplate instances
-- | Simulate ⊥ in logics that do not have it
false = Not $ Impl (Atom bot) (Atom bot)
-- | Simulate ⊤ in logics that do not have it
true  = Impl (Atom bot) (Atom bot)



embedClassicalIntuitionistic :: Formula' a -> Formula' a
-- glivenko's translation
embedClassicalIntuitionistic (Not f) =
  -- because not not not = not
  Not f
embedClassicalIntuitionistic f =
  if C.isPositive f
  then f
  else Not $ Not f

class HasBottom a where  
  bot :: a

instance HasBottom Int where
  bot = 0

instance HasBottom String where
  bot = "⊥"

instance (HasBottom a, HasBottom b) => HasBottom (a, b) where
  bot = (bot, bot)

embedIntuitionisticMinimal :: HasBottom a => Formula' a -> Formula' a
embedIntuitionisticMinimal (Atom a) = Atom a
embedIntuitionisticMinimal (Not f)
  = Impl (embedIntuitionisticMinimal f) (Atom bot)
embedIntuitionisticMinimal f
  = f { right = embedIntuitionisticMinimal $ right f,
        left  = embedIntuitionisticMinimal $ left f }


class Next a where
  -- | Concretely, @'next' x@ just constructs another element of the
  -- same type of @x@. In our case we expect (but not enforce) an
  -- invariant that @next x@ is a fresh element (i.e., it has not
  -- existed before).
  next :: a -> a


instance Next Int where
  next = (+1)

instance Next String where
  next s = '.' : s

instance (Next a, Next b) => Next (a,b) where
  next (a, b) = (next a, next b)


embedMinimalMinimalImplicational :: forall a. (HasBottom a, Next a, Ord a)
  => Formula' a -> M.Formula' a
embedMinimalMinimalImplicational initialFormula
  = foldr (\f r -> foldr M.Impl r $ expand f) initialFormula' newFs
  where
    (_, newsM, initialFormula')
      = subst (next $ findMax initialFormula) M.empty initialFormula
    subst i m f
      = case f of
      Atom a -> (i, M.insert f (M.Atom a) m, M.Atom a)
      Not _  -> error "Formula in minimal logic may not have negation"
      Impl _ _ -> go (\l r j -> (j, M.Impl l r))
      _ -> go (\_ _ j -> (next j, M.Atom j))
      where
        go g = let (i' , m', l)  = subst i m (left f)
                   (i'', m'', r) = subst i' m' (right f)
                   (i''', f') = g l r i''
                   (maybef, m''') = M.insertLookupWithKey (\_ _ ov -> ov)
                                                          f f' m''
               in (i''', m''', fromMaybe f' maybef)
    subFormulas = snd <$> newFs
    newFs = M.toList newsM
    newF f = newsM M.! f
    expand (Atom _, _) = []
    expand (Impl _ _, _) = []
    -- a ∧ b => [a -> b -> q, q -> a, q -> b], but we may omit all but
    -- the first one (right?)
    expand (And l r, q) = [
      M.Impl l' $ M.Impl r' q,
      M.Impl q l',
      M.Impl q r' ]
      where
        l' = newF l
        r' = newF r
    -- a ∨ b => [a -> q, b -> q]
    --- ++ [(a -> c) -> (b -> c) -> q -> c | c is subformula of original formula]
    expand (Or l r, q) =
      [M.Impl l' q, M.Impl r' q] ++ [
      M.Impl (M.Impl l' sf) (M.Impl (M.Impl r' sf) (M.Impl q sf))
      | sf <- subFormulas ]
      where
        l' = newF l
        r' = newF r
    expand _ = error "Internal error: broken invariant"
    findMax (Atom a) = a
    findMax f = max (findMax $ left f) (findMax $ right f)


embedClassicalMinimalImplicational :: (HasBottom a, Next a, Ord a)
  => Formula' a -> M.Formula' a
-- | @'embedClassicalMinimalImplicational' f@: simulate classical
-- formula @f@ into minimal implicational logic. The embedding is
-- based on the paper \'Exponentially Huge Natural Deduction proofs
-- are Redundant: Preliminary results on M⊃\', by Edward Hermann
-- Haeusler (available at <https://arxiv.org/abs/2004.10659>)
embedClassicalMinimalImplicational
  = embedMinimalMinimalImplicational
  . embedIntuitionisticMinimal
  . embedClassicalIntuitionistic
