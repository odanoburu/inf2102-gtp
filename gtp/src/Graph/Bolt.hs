{-|
Module      : Graph.Bolt
Description : Interact with graph backend
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare functions interacting with the proof backend. Depends heavily
on the "Database.Bolt" module from the @hasbolt@ library.
-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes #-}

-- # graph invariants
-- - each goal has only one incoming edge with goal=true
-- - ded relations always points to formula node

module Graph.Bolt (
  (=!),
  BoltCfg(..),
  -- * Identifiers for graph nodes
  --
  -- | Every node and relationship in the graph backend has an
  -- identifier. All identifiers are integers, but creating @newtype@s
  -- for them makes the code safer and easier to document.
  FormulaId(..),
  GoalId(..),
  NodeId(..),
  RootId(..),
  -- * Data types used to provide information to the REST API server
  --
  -- | These data types are not exact representations of the data in
  -- the backend; they are closer to how the frontend sees them.
  NodeKind(..),
  ProofNode(..),
  ReqResult(..),
  -- * Open goals
  --
  -- | The three functions below have the same output type, differing
  -- only by their input
  remainingGoals,
  remainingGoalsByLeafOrGoal,
  remainingGoalsByRoot,
  -- * Other
  --
  -- | Other functions provided by the module
  bolt,
  boltTransact,
  clearGraph,
  cloneNode,
  deleteProof,
  discardHypothesisCloseBranch,
  goalHypotheses,
  goalTypeCrown,
  graphStats,
  implicationElimBackward,
  implicationElimBackwardInteractive,
  implicationIntroBackward,
  implicationIntroBackwardInteractive,
  loadFormula,
  loadRoot,
  loadRootFormula,
  maybeResult,
  oneResult,
  possibleEliminators,
  proofTree,
  revertProof,
  showRoots,
  testBoltConnection,
  ) where

import Control.Exception (try)
import Control.Monad (unless)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader (asks)
import Data.Aeson ( ToJSON(..), Options(..), defaultOptions
                  , genericToJSON)
import qualified Data.Aeson as A
import Data.Coerce (coerce) -- TODO: make this safer, don't allow
                            -- nodeId to be coerced to goal, but the
                            -- contrary yes
import Data.Map (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (fromJust, isJust)
import Data.String.Interpolate (i)
import Data.Text (Text)
import qualified Data.Text as T
import Database.Bolt hiding (Node)
--import Debug.Trace (trace)
import GHC.Generics (Generic)
import Network.Connection (HostCannotConnect)
import Servant (FromHttpApiData)


import Logic.MIMP (Formula, Formula'(..), parse)
import Graph.Lib (App, Config(..))

-- for pretty-printing bolt values (e.g., statistics we get from a
-- cypher query)
deriving instance Generic Value
instance ToJSON Structure where
  toJSON _ = A.String "<Structure>"
deriving instance ToJSON Value


type Query = Text
type Params = Map Text Value

type NodeId' = Int
-- | general identifier, for any kind of node
newtype NodeId = NodeId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Show, ToJSON)
-- | identifier for goal nodes
newtype GoalId = GoalId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Read, Show, ToJSON)
-- | Identifier for root nodes
newtype RootId = RootId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Read, Show, ToJSON)
-- | Identifier for formula nodes
newtype FormulaId = FormulaId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Show, ToJSON)

instance RecordValue NodeId where
  exact (I nid) = pure $ NodeId nid
  exact x     = fail $ show x ++ " is not a NodeId value"
instance RecordValue GoalId where
  exact (I gid) = pure $ GoalId gid
  exact x     = fail $ show x ++ " is not a GoalId value"
instance RecordValue RootId where
  exact (I rid) = pure $ RootId rid
  exact x     = fail $ show x ++ " is not a RootId value"
instance RecordValue FormulaId where
  exact (I fid) = pure $ FormulaId fid
  exact x     = fail $ show x ++ " is not a FormulaId value"

-- | Kinds of proof nodes
data NodeKind = Leaf | Rule Text | Goal
  deriving (Eq, Show)

instance ToJSON NodeKind where
  toJSON Leaf = "Leaf"
  toJSON (Rule rule) = toJSON rule
  toJSON Goal = "Goal"

data ProofNode -- | Data type for proof nodes
  = ProofNode { _id :: NodeId -- ^ node identifier
              , formula :: Text -- ^ subformula derived by this node
              , kind :: NodeKind -- ^ node kind
              , role :: Text -- ^ role played by this node (usually the rule name)
              , children :: Maybe [ProofNode] -- ^ optionally: children nodes
              }
  deriving (Generic, Show)

aesonOptions :: Options
aesonOptions = defaultOptions { omitNothingFields = True }

instance ToJSON ProofNode where
  toJSON = genericToJSON aesonOptions

-- | Helper data type used to provide results of requests made to the
-- REST API
data ReqResult a
  = ReqResult { node :: NodeId -- ^ Node resulted by the request
              , result :: a -- ^ Result of the request
              }
  deriving (Generic, ToJSON)


formulaNodesQuery :: RootId -> Formula -> Text
-- | return query that inserts root formula in graph
formulaNodesQuery (RootId rootId) = T.intercalate "\nUNION ALL\n" . go
  --- we use rootId to differentiate the formula nodes from one graph
  --- from those of another
  -- OPTIMIZE: might be able to optimize with
  -- https://neo4j.com/docs/cypher-manual/current/clauses/call-subquery/
  -- or maybe just use loadcsv…
  where
    go :: Formula -> [Text]
    go (Atom _) = []
    go n@(Impl left right)
      = [i|MERGE (l:Formula{name: "#{left}", root: #{rootId}})
MERGE (r:Formula{name: "#{right}", root: #{rootId}})
MERGE (n:Formula{name: "#{n}", root: #{rootId}})
MERGE (l)-[:Builds{name: "ant"}]->(n)<-[:Builds{name: "csq"}]-(r)
RETURN n|] : concatMap go [left, right]


loadFormula :: Formula -> BoltActionT IO (RootId, GoalId)
-- | Load formula into graph backend, return the identifiers of the
-- root node and the formula goal.
--
-- Every proof graph is composed of formula nodes and derivation
-- nodes; every derivation notes points to the formula node that it
-- derives. 'loadFormula' creates the formula nodes and the
-- scaffolding for the derivation nodes to be created as the proof is
-- carried out.
loadFormula rootFormula = transact $ do
  (rootId, goalId) <- oneResult "createRoot"
                      (\r -> (r =! "rootId", r =! "goalId"))
                      createRootQuery params
  unless (isAtomic rootFormula) $
         query_ $ formulaNodesQuery rootId rootFormula
  return (rootId, goalId)
  where
    isAtomic (Atom _) = True
    isAtomic _ = False
    createRootQuery = [i|CREATE (crown:Meta {name: "crown"})-[:Meta]->(root:Rule {name: "root"})-[:DerivedBy {name: "root", goal: true}]->(g:Meta {name: "goal"})-[:Meta]->(crown)
CREATE (n:Formula {name: $rootFormula})
SET n.root = id(root)
CREATE (g)-[:Derives]->(n)
RETURN id(root) AS rootId, id(g) AS goalId|]
    params = props ["rootFormula" =: show rootFormula]

loadRootFormula :: String -> BoltActionT IO (Either String (RootId, GoalId))
-- | Same as 'loadFormula', but takes formula as a string, parses it,
-- and either reports errors or passes the result formula on to
-- 'loadFormula'
loadRootFormula
  = either (return . Left) (fmap Right . loadFormula) . parse


loadRoot :: String -> App IO (Either String (ReqResult GoalId))
-- | Same as 'loadRootFormula', but in the 'App' monad, and with
-- result appropriate for the REST API
loadRoot rootTxt
  = bolt
  $ fmap (uncurry ReqResult) . coerce
  <$> loadRootFormula rootTxt


showRoots :: App IO [(RootId, Text)]
-- | Return proofs available in the graph backend. The output is a
-- list of pairs (root identifier, formula string).
showRoots = boltTransact $ fmap go <$> query showProofsQuery
  where
    go r = (r =! "rootId", r =! "rootFormula")
    showProofsQuery =
      [i|MATCH (root:Rule {name: "root"})-[:DerivedBy]->(n)-[:Derives]->(f:Formula)
RETURN id(root) AS rootId, f.name as rootFormula|]

(=:?) :: IsValue a => Text -> Maybe a -> (Text, Value)
(=:?) key val = (key, maybe (toValue ()) toValue val)

(=!) :: RecordValue a => Record -> Text -> a
-- | Helper function to extract value from record
(=!) r key = fromJust . exact $ r M.! key

implicationIntroBackward :: Maybe GoalId -> BoltActionT IO [(NodeId, GoalId, Text)]
-- | Apply backward implication introduction rule to user-provided
-- node ID
implicationIntroBackward mId
  = fmap go
  <$> queryP "CALL custom.gtp.implicationIntroBackward($goalId) YIELD nodeId, goalId, goalFormula" params
  where
    params = props ["goalId" =:? mId]
    go r = (r =! "nodeId", r =! "goalId", r =! "goalFormula")

leafOrGoal :: Maybe NodeId -> NodeKind
leafOrGoal = maybe Goal (const Leaf)


implicationIntroBackwardInteractive :: GoalId -> App IO (ReqResult ProofNode)
-- | Same as 'implicationIntroBackward', plus possibly discard
-- hypothesis; provide output useful for REST API server
implicationIntroBackwardInteractive goalId = boltTransact go
  where
    go = do
      xs <- implicationIntroBackward $ Just goalId
      case xs of
        [(nodeId, newGoalId, formulaTxt)] -> do
          maybeLeaf <- discardHypothesisCloseBranch newGoalId
          return $ ReqResult nodeId (ProofNode { _id = coerce newGoalId
                                               , formula = formulaTxt
                                               , kind = leafOrGoal maybeLeaf
                                               , role = "pred"
                                               , children = Nothing})
        _ -> fail "Internal error: query should have exactly one result"


oneResult :: String -> (Record -> a) -> Query -> Params -> BoltActionT IO a
-- | Helper function for performing queries over the BOLT protocol
-- that only return one result row
oneResult name f queryTxt params = do
  result <- maybeResult name f queryTxt params
  case result of
    Nothing -> fail $ unwords [ "Internal error: query"
                              , name
                              , "with params"
                              , show $ M.toList params
                              , "must have exactly one result"]
    Just r -> return r


maybeResult :: String -> (Record -> a) -> Query -> Params -> BoltActionT IO (Maybe a)
-- | Helper function for performing queries over the BOLT protocol
-- that return at most one result row
maybeResult name f queryTxt params = do
  rs <- queryP queryTxt params
  case rs of
    [] -> return Nothing
    [r] -> return . Just $ f r
    _ -> fail $ unwords [ "Internal error: query"
                        , name
                        , "with params"
                        , show $ M.toList params
                        , "shouldn't have more than one result"]

cloneNode :: NodeId -> BoltActionT IO NodeId
-- | Clone node in the backend and return id of clone
cloneNode nodeId = oneResult "cloneNode" (=! "cloneId") queryTxt params
  where
    params = props ["nodeId" =: nodeId]
    queryTxt = "CALL custom.gtp.cloneNodeById($nodeId) YIELD cloneId"

implicationElimBackward :: GoalId -> FormulaId
  -> BoltActionT IO (NodeId, GoalId, Text, GoalId, Text)
-- | Apply backward implication elimination rule to user-provided node
-- ID, using input hypothesis
implicationElimBackward goalId etorId
  = oneResult "implicationElimBackward"
              (\r -> ( r =! "ruleId", r =! "etedGoalId", r =! "etedFormula"
                     , r =! "etorGoalId", r =! "etorFormula"))
              call params
  where
    params = props
      [ "goalId" =: goalId
      , "etorId" =: etorId]
    call = "CALL custom.gtp.implicationElimBackward($goalId, $etorId)"


implicationElimBackwardInteractive :: GoalId -> FormulaId -> App IO (ReqResult (ProofNode, ProofNode))
-- | Same as 'implicationElimBackward', but also check for possible
-- hypothesis discards and return value useful for REST API server
implicationElimBackwardInteractive goalId etorId = boltTransact go
  where
    go = do
      (nodeId, leftGoalId, leftFormula, rightGoalId, rightFormula) <- implicationElimBackward goalId etorId
      maybeLeftLeaf  <- discardHypothesisCloseBranch leftGoalId
      maybeRightLeaf <- discardHypothesisCloseBranch rightGoalId
      return ReqResult { node = nodeId
                       , result = (ProofNode { _id = coerce leftGoalId
                                             , formula = leftFormula
                                             , kind = leafOrGoal maybeLeftLeaf
                                             , role = "eted"
                                             , children = Nothing}
                                  , ProofNode { _id = coerce rightGoalId
                                              , formula = rightFormula
                                              , kind = leafOrGoal maybeRightLeaf
                                              , role = "etor"
                                              , children = Nothing})}

possibleEliminators :: GoalId -> BoltActionT IO [(FormulaId, Text)]
-- OPTIMIZE: this will return all antecedents of the formula deduced
-- by the goal, which might be a lot; so we would like to stream the
-- results, or return them in chunks (the interactive interface would
-- show only some, while the automatic one would consume the chunks,
-- try to prove them, and ask for more if none end up working)
-- | Return all possible eliminators for goal identifier. Returns list
-- of pairs (formula node id, formula string)
possibleEliminators goalId = fmap go <$> queryP theQuery params
  where
    params = props ["goalId" =: goalId]
    go r = (r =! "etorId", r =! "etor")
    theQuery =
      [i|MATCH (g:Meta {name: "goal"})-[:Derives]->(pred:Formula)-[:Builds {name: "csq"}]->(eted:Formula)<-[:Builds {name: "ant"}]-(etor:Formula)
WHERE id(g)=$goalId
RETURN etor.name AS etor, id(etor) AS etorId|]


discardHypothesisCloseBranch :: GoalId -> BoltActionT IO (Maybe NodeId)
-- | Check if goal specified by identifier can be discarded as a
-- hypothesis. Return nodeId of leaf if discard happened
discardHypothesisCloseBranch goalId =
  oneResult "discardHypothesis" (=! "leafId")
              "CALL custom.gtp.discardHypothesis.maybe($goalId) YIELD leafId"
              params
  where
    params = props ["goalId" =: goalId]


goalHypothesesQuery :: Text
goalHypothesesQuery =
  [i|MATCH (goal:Meta {name: "goal"})
WHERE id(goal)=$goalId
WITH goal
MATCH (hyp:Formula)-[:Builds {name: "ant"}]->(imp)<-[:Derives]-(impIntro:Rule)-[:DerivedBy {name: "hyp"}]->(goal)
RETURN id(hyp) as hypothesisId, hyp.name as hypothesisFormula|]


goalHypotheses :: GoalId -> App IO [(FormulaId, Text)]
-- | Return hypotheses available at goal specified by identifier
goalHypotheses goalId = bolt $ fmap go <$> queryP goalHypothesesQuery params
  where
    params = props ["goalId" =: goalId]
    go r = (r =! "hypothesisId", r =! "hypothesisFormula")


proofTree :: Int -> RootId -> App IO [ProofNode]
-- | @'proofTree' maxLevel rootId@: Return proof tree rooted at node
-- identified by @rootId@, up to @maxLevel@ levels. The root node can
-- be the root of any subproof, not necessarily the root of whole
-- proof.
proofTree maxLevel rootId = boltTransact $ go maxLevel (coerce rootId)
  where
    getNodeQuery =
      [i|MATCH (parent:Rule)-[db:DerivedBy]->(node)-[:Derives]->(f:Formula)
WHERE id(parent) = $parentId
RETURN id(node) AS nodeId, f.name AS nodeFormula, node.name AS nodeKind, db.name AS role|]
    go :: Int -> NodeId -> BoltActionT IO [ProofNode]
    go level nodeId = do
      let params = props ["parentId" =: nodeId]
      children <- fmap buildNode <$> queryP getNodeQuery params
      if level == 0
        then return children
        else mapM addChildren children
      where
        readNodeKind :: Text -> NodeKind
        readNodeKind "leaf" = Leaf
        readNodeKind "goal" = Goal
        readNodeKind r = Rule r
        buildNode r = ProofNode (r =! "nodeId") (r =! "nodeFormula")
                                (readNodeKind $ r =! "nodeKind") (r =! "role") Nothing
        addChildren n@ProofNode{_id} = do
          children <- go (level - 1) _id
          return n{children = if null children then Nothing else Just children}


deleteProof :: RootId -> App IO Bool
-- FIXME: doesn't delete formula nodes
-- | Delete proof with root in the specified identifier.
deleteProof rootId
  = boltTransact
  $ isJust
  <$> maybeResult "deleteRoot" ((=! "deleted") :: Record -> Bool)
                  deleteProofQuery params
  where
    deleteProofQuery = [i|MATCH (root:Rule {name: "root"})-[:DerivedBy*]->(n)
WHERE id(root)=$rootId
DETACH DELETE n
WITH collect(n) as dels, root
MATCH (crown:Meta {name: "crown"})-[:Meta]->(root)
DETACH DELETE root
WITH crown
DELETE crown
RETURN true AS deleted|]
    params = props ["rootId" =: rootId]


revertProof :: NodeId -> App IO NodeId
-- | Revert subproof rooted at node with the input identifier. This
-- reverses all the proof steps (rule applications) performed below
-- this node.
revertProof nodeId = boltTransact $ do
  goalId <- go <$> queryP createGoalQuery nodeParams
  let goalParams = props ["goalId" =: goalId]
  _ <- queryP_ undiscardRemoveDupsHyposQuery goalParams
  return goalId
  where
    go [r] = r =! "goalId"
    go _ = error "Internal error: there should be exactly one goal node"
    createGoalQuery = [i|// create goal node
CREATE (g:Meta{name: "goal"})
WITH g
MATCH (n)-[:Derives]->(f)
WHERE id(n) = $nodeId
WITH f, g, n
// get path from node to be reverted from until leaves
MATCH p=(n)-[:DerivedBy*]->(leafOrGoal)
WHERE NOT (leafOrGoal)-[:DerivedBy]->()
WITH f, g, leafOrGoal, p
MATCH (leafOrGoal)-[:Meta]->(crown:Meta {name: "crown"})
WITH crown, f, g, p
UNWIND nodes(p) as m
// merge nodes in path to goal node
CALL apoc.refactor.mergeNodes([g, m], {properties: "discard"}) YIELD node
WITH collect(node) as pnodes, g, f, crown
// delete outgoing relationships (self-relationships and spurious :Derives)
MATCH (g)-[r]->()
DELETE r
WITH crown, f, g, collect(r) as _
// recreate derives relation
CREATE (crown)<-[:Meta]-(g)-[:Derives {name: "ded"}]->(f)
WITH g
// add goal prop to derivedby edge
MATCH ()-[db:DerivedBy]->(g)
WHERE db.name <> "hyp"
SET db.goal = true
RETURN id(g) AS goalId
|]
    undiscardRemoveDupsHyposQuery = [i|// undiscard hypotheses
MATCH ()-[hyp:DerivedBy {name: "hyp"}]->(g)
WHERE id(g) = $goalId
SET hyp.discarded = false
WITH g
// remove duplicate hypotheses
MATCH (m)-[r]->(g)<-[r2]-(m)
WHERE r.name = r2.name AND id(r) < id(r2)
DELETE r2
RETURN g|]
    nodeParams = props ["nodeId" =: nodeId]


remainingGoals :: NodeId -> BoltActionT IO [GoalId]
-- | Return open goals attached to crown with the input node
-- identifier. Note that some of these goals may not need to be closed
-- if there is a competition between branches.
remainingGoals crownId
  = fmap (=! "goalId")
  <$> queryP getGoals params
  where
    params = props ["crownId" =: crownId]
    getGoals =
      [i|MATCH (crown:Meta {name: "crown"})
WHERE id(crown) = $crownId
WITH crown
MATCH (goal:Meta {name: "goal"})-[:Meta]->(crown)
RETURN id(goal) AS goalId|]


remainingGoalsByLeafOrGoal :: NodeId -> BoltActionT IO [GoalId]
-- this function may be called with nodes that are not leaves nor
-- goals, in this case the return is []
-- | Return open goals in the proof to which the input identifier
-- belongs.
remainingGoalsByLeafOrGoal leafOrGoalId = do
  maybeCrownId <- maybeResult "getCrownFromLeafOrGoal" (=! "crownId")
                              getCrown params
  case maybeCrownId of
    Nothing -> return []
    Just crownId -> remainingGoals crownId
  where
    params = props ["leafOrGoalId" =: leafOrGoalId]
    getCrown =
      [i|MATCH (leafOrGoal:Meta)-[:Meta]->(crown:Meta {name: "crown"})
WHERE id(leafOrGoal) = $leafOrGoalId AND leafOrGoal.name IN ["leaf", "goal"]
RETURN id(crown) AS crownId|]


remainingGoalsByRoot :: RootId -> BoltActionT IO [GoalId]
-- | Return open goals in the proof rooted in the node with the input
-- identifier.
remainingGoalsByRoot rootId = do
  maybeCrownId <- maybeResult "getCrownFromRoot" (=! "crownId")
                              getCrown params
  maybe (return []) remainingGoals maybeCrownId
  where
    params = props ["rootId" =: rootId]
    getCrown =
      [i|MATCH (root:Rule {name: "root"})<-[:Meta]-(crown:Meta {name: "crown"})
WHERE id(root) = $rootId
RETURN id(crown) AS crownId|]


goalTypeCrown :: GoalId -> BoltActionT IO (NodeId, Bool)
-- return id of crown node, plus if the goal is directly under the
-- root (we use this in the ATP module, because we apply different
-- techniques to regular goals and those under the root)
-- | Return identifier of crown node and boolean indicating if the
-- input goal identifier is that of a root goal.
--
-- The crown node is connected to all goals, leaves, and root of the
-- proof.
goalTypeCrown goalId = oneResult "goalTypeCrown" (\r -> (r =! "crownId", r =! "isRoot")) theQuery params
  where
    theQuery =
      [i|MATCH (goal:Meta {name: "goal"})-[:Meta]->(crown:Meta {name: "crown"})
WHERE id(goal) = $goalId
WITH crown, goal
OPTIONAL MATCH (root:Rule {name: "root"})-->(goal)
RETURN id(crown) as crownId, root IS NOT NULL AS isRoot|]
    params = props ["goalId" =: goalId]


clearGraph :: App IO ()
-- | Delete everything in the graph backend.
clearGraph
  = boltTransact
  $ query_ "MATCH (n) DETACH DELETE n"


graphStats :: App IO (Map Text Value)
-- | Return statistics about the graph backend.
graphStats
  = boltTransact
    $ oneResult "graph-statistics" (=! "stats")
    [i|CALL apoc.meta.stats() YIELD stats
RETURN stats AS stats|] mempty


testBoltConnection :: App IO ()
-- | Test BOLT protocol connection to graph backend.
testBoltConnection = bolt $ query_ "RETURN true"


bolt :: BoltActionT IO a -> App IO a
-- | Lift 'BoltActionT' monad to 'App' monad
bolt action = do
  boltCfg <- asks boltC
  pipeOrErr <- liftIO . try $ connect boltCfg
  case pipeOrErr of
    Right pipe
      -> do
      result <- liftIO $ run pipe action
      liftIO $ close pipe
      return result
    Left (_ex :: HostCannotConnect) -> error "can't connect to database."


boltTransact :: BoltActionT IO a -> App IO a
-- | Lift 'BoltActionT' monad to 'App' monad, performing the bolt
-- actions as a transaction
boltTransact = bolt . transact

