#!/bin/bash

DATE=$(date '+%Y-%m-%d %H:%M:%S')

if [ "${1}" == "dev" ]
then
    GTP_PORT=2488
    PROG="gtp-dev"
elif [ "${1}" == "prod" ]
then
    GTP_PORT=2487
    PROG="gtp-prod"
else
    echo "Argument must be one of 'dev' or 'prod'" | systemd-cat -p info;
    exit 1
fi
echo "gtp started at port ${GTP_PORT} on ${DATE}" | systemd-cat -p info
"$PROG" --help | systemd-cat -p info
"$PROG" serve -p "${GTP_PORT}"

