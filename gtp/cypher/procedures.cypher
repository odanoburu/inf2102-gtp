// -*- mode: prog; comment-start: "// "; indent-tabs-mode: nil -*-

//** general **//

// OPTIMIZE: return NODEs instead of INTs (is this really an
// optimization?)

/* clone node */
CALL apoc.custom.asProcedure("gtp.cloneNodeById",
     'MATCH (node)
WHERE id(node) = $nodeId
// duplicate node
CALL apoc.refactor.cloneNodes([node], true) // true to copy relations
  YIELD output AS clone
RETURN id(clone) AS cloneId',
       "write",
       [["cloneId", "INT"]],
       [["nodeId", "INT"]]);

/* backward implication introduction */
// TODO: remove "ded" name from :Derives edge since all derives are
// "ded"
CALL apoc.custom.asProcedure("gtp.implicationIntroBackward",
     '// matches any node which is an implication formula and is a goal
// must specify name in :Builds or else there will be two matches for each node (and the query will fail)
MATCH (prev:Rule)-[m:DerivedBy {goal: true}]->(goal:Meta {name: "goal"})-[r:Derives]->(ded:Formula)<-[:Builds {name: "ant"}]-(:Formula)
// user may or may not specify node; if she doe not, then all matching nodes have the rule applied to them
WHERE id(goal)=$goalId
DELETE r  // temporary edge is deleted
WITH prev, goal, ded, m
// introduce implication node connected to previous derivation node
CREATE (prev)-[:DerivedBy {name: m.name}]->(n:Rule {name: "impIntro"})-[:Derives {name: "ded"}]->(ded)
DELETE m  // node is no longer goal
WITH ded, n, goal
// get formula consequent
MATCH (ded)<-[:Builds {name: "csq"}]-(right:Formula)
// create new goal
CREATE (n)-[:DerivedBy {name: "pred", goal: true}]->(goal)
// create hypothesis edge; add edge to derived formula
CREATE (n)-[:DerivedBy {name: "hyp", discarded: false}]->(goal)-[:Derives]->(right)
RETURN id(n) AS nodeId, id(goal) AS goalId, right.name AS goalFormula',
       "write",
       [["nodeId", "INT"], ["goalFormula", "STRING"], ["goalId", "INT"]],
       [["goalId", "INT"]]);
// FIXME: don't hardcode names
/// how to treat the undiscarded hypothesis is not so clear to me,
/// yet (maybe just mark them as undiscarded?)


/* backward implication elimination */
CALL apoc.custom.asProcedure("gtp.implicationElimBackward.createRule",
     '// matches correct goal node
MATCH (prev:Rule)-[m:DerivedBy {goal: true}]->(goal:Meta {name: "goal"})-[r:Derives]->(ded:Formula)
// user must specify ID
WHERE id(goal)=$goalId
DELETE r  // temporary relation
WITH ded, goal, m, prev
// create derivation node pointing to deduced formula
CREATE (prev)-[:DerivedBy {name: m.name}]->(impElim:Rule {name: "impElim"})-[:Derives {name: "ded"}]->(ded)
DELETE m  // previous node no longer a goal
RETURN id(impElim) AS ruleId, id(ded) AS dedId',
       "write",
       [["ruleId", "INT"], ["dedId", "INT"]],
       [["goalId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.implicationElimBackward.attachGoals",
     '// match goals
MATCH (etedGoal:Meta {name: "goal"})
WHERE id(etedGoal) = $etedGoalId
WITH etedGoal
MATCH (etorGoal:Meta {name: "goal"})
WHERE id(etorGoal) = $etorGoalId
WITH etedGoal, etorGoal
MATCH (impElim:Rule)
WHERE id(impElim) = $ruleId
WITH etedGoal, etorGoal, impElim
// deduced formula is the consequent of the eliminated formula
MATCH (etor:Formula)-[:Builds {name: "ant"}]->(eted:Formula)<-[:Builds {name: "csq"}]-(ded)
WHERE id(etor) = $etorId AND id(ded) = $dedId
// create links from goals to rule node
CREATE (impElim)-[:DerivedBy {name: "eted", goal: true}]->(etedGoal)-[:Derives]->(eted)
CREATE (impElim)-[:DerivedBy {name: "etor", goal: true}]->(etorGoal)-[:Derives]->(etor)
RETURN eted.name as etedFormula, etor.name as etorFormula',
       "write",
       [["etedFormula", "STRING"], ["etorFormula", "STRING"]],
       [["etorGoalId", "INT"], ["etedGoalId", "INT"], ["dedId", "INT"],
        ["etorId", "INT"], ["ruleId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.implicationElimBackward",
     'CALL custom.gtp.implicationElimBackward.createRule($goalId)
  YIELD ruleId, dedId
CALL custom.gtp.cloneNodeById($goalId) YIELD cloneId AS etedGoalId
CALL custom.gtp.implicationElimBackward.attachGoals($goalId, etedGoalId,
     dedId, $etorId, ruleId) YIELD etedFormula, etorFormula
RETURN ruleId, etedGoalId, etedFormula, $goalId AS etorGoalId, etorFormula',
       "write",
       [["ruleId", "INT"], ["etedGoalId", "INT"], ["etedFormula", "STRING"],
        ["etorGoalId", "INT"], ["etorFormula", "STRING"]],
       [["goalId", "INT"], ["etorId", "INT"]]);


/* discard hypothesis and close branch */
CALL apoc.custom.asProcedure("gtp.discardHypothesis.removeUnused",
     // delete unused hypotheses
     'MATCH (impIntro)-[h:DerivedBy {name: "hyp", discarded: false}]->(leaf:Meta {name: "leaf"})
WHERE id(leaf)=$leafId
DELETE h
WITH collect(h) AS __
RETURN NULL AS _',
       "write",
       [["_", "ANY"]],
       [["leafId", "INT"]]);


CALL apoc.custom.asProcedure("gtp.discardHypothesis.closeBranch.maybe",
     '// match hypothesis to goal
OPTIONAL MATCH (hyp)-[:Builds {name: "ant"}]->(imp)<-[rg:Derives]-(impIntro)-[h:DerivedBy {name: "hyp"}]->(g:Meta {name: "goal"})-[:Derives]->(hyp)
WHERE id(g)=$goalId
// mark hypothesis as discarded
SET h.discarded = true
// turn goal into leaf
SET rg.goal = null
SET g.name = "leaf"
RETURN g IS NOT NULL AS ok, id(g) AS leafId, id(impIntro) AS nodeId
LIMIT 1', // DOUBT: should only discard one hypothesis, right?
       "write",
       [["ok", "BOOL"], ["leafId", "INT"], ["nodeId", "INT"]],
       [["goalId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.discardHypothesis.maybe",
     'CALL custom.gtp.discardHypothesis.closeBranch.maybe($goalId) YIELD ok, leafId
CALL apoc.do.when(ok, "CALL custom.gtp.discardHypothesis.removeUnused($leafId) YIELD _ RETURN _", "", {leafId: leafId}) YIELD value AS _
RETURN ok, leafId AS leafId',
     "write",
     [["ok", "BOOL"], ["leafId", "INT"]],
     [["goalId", "INT"]]);


//** ITP **//



//** ATP **//


/* makeCompetition */
CALL apoc.custom.asProcedure("gtp.atp.addCompeting",
     'CREATE (competing:Meta {name: "competing"})
WITH competing
UNWIND $ruleIds as ruleId
MATCH (ruleNode)
WHERE id(ruleNode) = ruleId
CREATE (competing)-[:Meta]->(ruleNode)
WITH competing, collect(ruleNode) as _
RETURN id(competing) as competingId',
       "write",
       [["competingId","INT"]],
       [["ruleIds", "LIST OF INT"]]);

CALL apoc.custom.asProcedure("gtp.atp.keepCompetingOrder",
     'MATCH (competing:Meta)
WHERE id(competing) = $competingId
WITH competing
MATCH (goal)-[:Meta]->(crown:Meta {name: "crown"})
WHERE id(goal) = $goalId
WITH competing, crown
OPTIONAL MATCH (crown)-[c:Meta]->(previousCompeting:Meta {name: "competing"})
WITH competing, crown, c
CREATE (crown)-[:Meta]->(competing)
WITH competing, c
CALL apoc.refactor.from(c, competing) YIELD output AS _
RETURN null as _',
       "write",
       [["_", "ANY"]],
       [["competingId", "INT"], ["goalId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.atp.makeCompetition",
     'CALL custom.gtp.atp.addCompeting($ruleIds) YIELD competingId
CALL custom.gtp.atp.keepCompetingOrder(competingId, $goalId) YIELD _
RETURN _',
     "write",
     [["_", "ANY"]],
     [["goalId", "INT"], ["ruleIds", "LIST OF INT"]]);



/* maybeCloseCompetition */
CALL apoc.custom.asProcedure("gtp.atp.hasWinner",
     'MATCH (leaf:Meta {name: "leaf"})-[:Meta]->(crown:Meta {name: "crown"})-[:Meta]->(competing:Meta {name: "competing"})
WHERE id(leaf) = $leafId
WITH competing, crown, leaf
MATCH (competing)-[:Meta]->(parent:Rule)-[:DerivedBy*]->(leaf)
WITH competing, parent
RETURN id(competing) AS competingId
  , CASE NOT EXISTS((parent)-[:DerivedBy*]->(:Meta {name: "goal"}))
     WHEN true THEN id(parent)
    END AS maybeWinnerId',
// OPTIMIZE: check if apoc might have something to help us here what
// we want to do is to search for a goal and if found stop the search
// there (while we currently find all terminating nodes, goals and
// leafs, and check if they are only leaves)
/// another idea is to use counters, but that might be fragile or slow
/// with all the locking that would be needed
     "write",
     [["competingId", "INT"], ["maybeWinnerId", "ANY"]], // ANY = INT or NULL
     [["leafId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.atp.closeLosing",
     'MATCH (competing)-[c:Meta]->(loser:Rule)
WHERE id(competing) = $competingId AND id(loser) <> $winnerId
DELETE c
WITH competing, loser
MATCH p = (loser)-[:DerivedBy*]->()
FOREACH (n IN nodes(p)| DETACH DELETE n)
WITH collect(p) as _, competing
MATCH (crown:Meta {name: "crown"})-[:Meta]->(competing)
OPTIONAL MATCH (competing)-[:Meta]->(otherCompeting:Meta)
WITH competing, crown, otherCompeting
DETACH DELETE competing
WITH crown, otherCompeting AS maybeOtherCompeting
UNWIND CASE maybeOtherCompeting IS NULL
  WHEN true THEN []
  ELSE [maybeOtherCompeting] END AS otherCompeting
CREATE (crown)-[:Meta]->(otherCompeting)
RETURN null AS _',
// neo4j v4 changes the semantics of UNWIND <thing that is not a
// list>, which would simplify this UNWIND CASE thing
       "write",
       [["_", "ANY"]],
       [["competingId", "INT"], ["winnerId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.atp.maybeCloseCompetition",
     'CALL custom.gtp.atp.hasWinner($leafId) YIELD competingId, maybeWinnerId
UNWIND CASE maybeWinnerId IS NULL
  WHEN true THEN []
  ELSE [maybeWinnerId] END AS winnerId
CALL custom.gtp.atp.closeLosing(competingId, winnerId) YIELD _
CALL custom.gtp.atp.maybeCloseCompetition($leafId) YIELD maybeWinnerId AS __
RETURN winnerId AS maybeWinnerId',
       "write",
       [["maybeWinnerId", "ANY"]],
       [["leafId", "INT"]]);



/* automatic backward implication elimination */
CALL apoc.custom.asProcedure("gtp.atp.implicationElimBackward",
     'CALL custom.gtp.implicationElimBackward($goalId, $elimId)
  YIELD etedGoalId, etorGoalId
CALL apoc.cypher.doIt("UNWIND $goals AS newGoalId
     CALL custom.gtp.discardHypothesis(newGoalId) YIELD leafId
     WITH collect(leafId) AS _
     RETURN _", {goals: [etedGoalId, etorGoalId]}) YIELD value AS _
RETURN etedGoalId, custom.gtp.isLeaf(etedGoalId) AS isEtedLeaf, etorGoalId, custom.gtp.isLeaf(etorGoalId) as isEtorLeaf',
     "write",
     [["etedGoalId", "INT"], ["isEtedLeaf", "BOOL"],
      ["etorGoalId", "INT"], ["isEtorLeaf", "BOOL"]],
     [["goalId", "INT"], ["elimId", "INT"]]);

/* goal reachability */
CALL apoc.custom.asProcedure("gtp.atp.goalReachable.byIntro",
     // FIXME: this doesn't work when the target formula is (A -> A)
     'MATCH (goal:Meta)-[:Derives]->(goalFormula:Formula)<-[:Builds {name: "csq"}]-(:Formula)-[:Builds {name: "ant"}]->(:Formula)<-[:Derives]-(:Rule {name: "impIntro"})-[:DerivedBy {name: "hyp"}]->(goal)
// goal is just reachable by implication introduction if its target
// formula’s consequent is the antecedent of an implication
// introduction
WHERE id(goal) = $goalId
RETURN true AS reachable
LIMIT 1',
        "read",
        [["reachable", "BOOL"]],
        [["goalId", "INT"]]);

CALL apoc.custom.asProcedure("gtp.atp.goalReachable.byElim",
     '// first get possible eliminator
MATCH (goal:Meta)-[:Derives]->(pred:Formula)-[:Builds {name: "csq"}]->(eted:Formula)<-[:Builds {name: "ant"}]-(etor:Formula)
WHERE id(goal) = $goalId
WITH goal, eted, etor
// check if is dischargeable
MATCH (goal:Meta)<-[:DerivedBy {name: "hyp"}]-(:Rule {name: "impIntro"})-[:Derives]->(:Formula)<-[:Builds {name: "ant"}]-(eted)
WITH goal, etor
MATCH (goal:Meta)<-[:DerivedBy {name: "hyp"}]-(:Rule {name: "impIntro"})-[:Derives]->(:Formula)<-[:Builds {name: "ant"}]-(etor)
RETURN id(etor) as etorId
LIMIT 1',
     "read",
     [["etorId", "INT"]],
     [["goalId", "INT"]]);


/* cycle detection */
CALL apoc.custom.asProcedure("gtp.atp.willCycleIf.impIntro",
     'MATCH (goal:Meta {name: "goal"})
WHERE id(goal) = $goalId
WITH goal
OPTIONAL MATCH (f:Formula)<-[:Derives]-(:Rule {name: "impElim"})-[:DerivedBy {name: "eted"}]->(goal)-[:Derives]->(:Formula)<-[:Builds {name: "csq"}]-(f)
RETURN CASE f WHEN null THEN false ELSE true END AS willCycle',
     "read",
     [["willCycle", "BOOL"]],
     [["goalId", "INT"]]);


/* automatic backward implication introduction */
CALL apoc.custom.asProcedure("gtp.atp.implicationIntroBackward",
     'CALL custom.gtp.implicationIntroBackward($goalId) YIELD goalId AS newGoalId
CALL custom.gtp.discardHypothesis.maybe(newGoalId) YIELD leafId AS maybeLeaf
RETURN newGoalId AS goalId, maybeLeaf IS NOT NULL AS isLeaf',
     "write",
     [["goalId", "INT"], ["isLeaf", "BOOL"]],
     [["goalId", "INT"]]);


/** utils **/
CALL apoc.custom.asFunction("gtp.isLeaf",
     'OPTIONAL MATCH (n)
WHERE id(n) = $nodeId
RETURN n IS NOT NULL AND n.name = "leaf" AS isLeaf',
       "BOOL",
       [["nodeId", "INT"]]);

CALL apoc.custom.asFunction("gtp.isGoal",
     'OPTIONAL MATCH (n)
WHERE id(n) = $nodeId
RETURN n IS NOT NULL AND n.name = "goal" AS isGoal',
       "BOOL",
       [["nodeId", "INT"]]);

CALL apoc.custom.asProcedure("test.returnRow",
     'CALL apoc.when($yes, "RETURN true AS r", "MATCH () WHERE false RETURN false AS r") YIELD value RETURN value.r AS r',
     "read",
     [["r", "BOOL"]],
     [["yes", "BOOL"]]);
