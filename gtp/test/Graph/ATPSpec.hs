module Graph.ATPSpec (spec) where

import Control.Monad.Reader (runReaderT)
import Test.Hspec

import Graph.BoltSpec (getConfig)
import Graph.ATP (ATPRes(..), solveFormula)
import Graph.Bolt (clearGraph, testBoltConnection)
import Graph.Lib (App)
import Logic.Lib (Logic(..), onFormula)


run :: App IO a -> IO a
run action = getConfig >>= runReaderT action

go :: Logic -> (ATPRes -> Bool) -> Int -> String -> IO Bool
go l f gas formula = run (onFormula l formula f')
  where
    f' (Left err) = fail err
    f' (Right x) = f <$> solveFormula gas x

isSolved :: ATPRes -> Bool
isSolved (Solved _) = True
isSolved _ = False

cantSolve :: ATPRes -> Bool
cantSolve (Unsolved _) = True
cantSolve _ = False

needsMoreGas :: ATPRes -> Bool
needsMoreGas NeedsMoreGas = True
needsMoreGas _ = False

fibonacci3 :: String
fibonacci3 = "((1 -> 2) -> ((2 -> 3) -> (1 -> 3)))"

spec :: Spec
spec =
  beforeAll (getConfig >>= runReaderT testBoltConnection)
    (describe "automatic proofs" $ do
      it "solves simple formulas"
        $ go MinimalImplicational isSolved 1 "(1 -> 1)" `shouldReturn` True

      it "can't do anything without gas"
        $ go MinimalImplicational needsMoreGas 0 fibonacci3
        `shouldReturn` True

      it "can't do much without the necessary gas"
        $ go MinimalImplicational needsMoreGas 1 fibonacci3 `shouldReturn` True

      it "fibonacci formula n = 3"
        $ go MinimalImplicational isSolved 4 fibonacci3 `shouldReturn` True

      it "fibonacci n = 5"
        $ go MinimalImplicational isSolved 10 "(-> (-> 1 2) (-> (-> 1 (-> 2 3)) (-> (-> 2 (-> 3 4)) (-> (-> 3 (-> 4 5)) (-> 1 5)))))" `shouldReturn` True

      it "doesn't solve impossible formulas"
        $ go MinimalImplicational cantSolve 10 "(A1 -> A2)" `shouldReturn` True

      it "doesn't solve impossible formulas (1)"
        $ go MinimalImplicational cantSolve 10 "A1" `shouldReturn` True

      -- it "big fibonacci formula"
      --   $ (isSolved <$> (bolt $ solveFormula 50 $ fibonacci 11)) `shouldReturn` True

      it "solves Łukasiewicz 1"
        $ go MinimalImplicational isSolved 2 "(-> p (-> q p))" `shouldReturn` True

      it "solves Łukasiewicz 2"
        $ go MinimalImplicational isSolved 4 "(-> (-> p (-> q r)) (-> (-> p q) (-> p r)))" `shouldReturn` True

      it "solves embedded classical formulas and elim left"
        $ go Classical isSolved 2 "(-> (& 1 2) 1)" `shouldReturn` True

      it "solves embedded classical formulas ant elim right"
        $ go Classical isSolved 2 "(-> (& 1 2) 2)" `shouldReturn` True

      it "solves embedded classical formulas and intro"
        $ go Classical isSolved 3 "(-> 2 (-> 1 (& 1 2)))" `shouldReturn` True

      it "solves embedded classical formulas or elim"
        $ go Classical isSolved 3 "(-> (-> 1 3) (-> (-> 2 3) (-> (| 1 2) 3)))" `shouldReturn` True

      it "solves embedded classical formulas or intro left"
        $ go Classical isSolved 2 "(-> 1 (| 1 2))" `shouldReturn` True

      it "solves embedded classical formulas or intro right"
        $ go Classical isSolved 2 "(-> 2 (| 1 2))" `shouldReturn` True

      it "hypothetical syllogism"
        $ go Classical isSolved 4 "(-> (& (-> p q) (-> q r)) (-> p r))"
          `shouldReturn` True

      it "and-commutation"
        $ go Classical isSolved 4 "(-> (& p q) (& q p))"
          `shouldReturn` True

      it "curry"
        $ go Classical isSolved 4 "(-> (-> (& p q) r) (-> p (-> q r)))"
          `shouldReturn` True

      it "uncurry"
        $ go Classical isSolved 4 "(-> (-> p (-> q r)) (-> (& p q) r))"
          `shouldReturn` True

      it "or tautology"
        $ go Classical isSolved 4 "(-> p (| p p))"
          `shouldReturn` True

      it "and tautology"
        $ go Classical isSolved 4 "(-> p (& p p))"
          `shouldReturn` True

      it "clears graph alright"
        $ (getConfig >>= runReaderT clearGraph) `shouldReturn` ())
