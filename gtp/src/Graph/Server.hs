{-|
Module      : Graph.Server
Description : Serve a REST API for proof graphs
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare a REST API for interacting with proof graphs through an HTTP
server.

When the server is running, issuing a GET request to @docs/@ will
return the documentation of the API.
-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Graph.Server (LogLevel(..), gtpServe) where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (ask, runReaderT, void)
import Data.ByteString.Lazy (ByteString)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8Builder)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Data.Text.Lazy (pack)
import Network.HTTP.Types (ok200)
import Network.Wai (responseLBS, responseBuilder)
import Network.Wai.Handler.Warp (Port, run)
import Network.Wai.Middleware.Cors (cors, corsRequestHeaders, simpleCorsResourcePolicy)
import Network.Wai.Middleware.RequestLogger (logStdoutDev, logStdout)
import Servant
import Servant.JS (jsForAPI, vanillaJS)
import Servant.Docs

import Graph.Bolt ( ProofNode(..), FormulaId(..), GoalId(..), NodeId(..), RootId(..), NodeKind(..), ReqResult(..), bolt, deleteProof, loadRoot, goalHypotheses
                  , implicationIntroBackwardInteractive, implicationElimBackwardInteractive
                  , possibleEliminators, proofTree, revertProof, showRoots)
import Graph.ATP (solveGoal)
import Graph.Auth (User, UserDB, checkBasicAuth)
import Graph.Lib (App, Config(..))


type UserAPI
  = "newProof" :> ReqBody '[JSON] String
       :> Post '[JSON] (Either String (ReqResult GoalId))
  :<|> "loadProof" :> Capture "rootId" RootId :> Capture "maxLevel" Int
       :> Get '[JSON] [ProofNode]
  :<|> "deleteRoot" :> Capture "rootId" RootId :> Get '[JSON] Bool
  :<|> "revertProof" :> Capture "nodeId" NodeId :> Get '[JSON] NodeId
  :<|> "showRoots" :> Get '[JSON] [(RootId, Text)]
  :<|> "impIntroB" :> Capture "goalId" GoalId
       :> Get '[JSON] (ReqResult ProofNode)
  :<|> "impElimB" :> Capture "goalId" GoalId :> Capture "eliminatorId" FormulaId
       :> Get '[JSON] (ReqResult (ProofNode, ProofNode))
  :<|> "solveGoal" :> Capture "gas" Int :> Capture "goalId" GoalId
       :> Get '[JSON] ()
  -- does nothing, just to check authentication
  :<|> "checkAuth" :> Get '[JSON] Bool
  :<|> "eliminators" :> Capture "goalId" GoalId :> Get '[JSON] [(FormulaId, Text)]
  :<|> "hypotheses" :> Capture "goalId" GoalId
       :> Get '[JSON] [(FormulaId, Text)]

type ProtectedUserAPI = BasicAuth "Proof information and manipulation" User
  :> "gtp" :> UserAPI

instance ToCapture (Capture "eliminatorId" FormulaId) where
  toCapture _ =
    DocCapture "eliminatorId"
               "(integer) ID of the formula to be used as the eliminator predicate in the implication elimination rule"

instance ToCapture (Capture "nodeId" NodeId) where
  toCapture _ =
    DocCapture "nodeId"
               "(integer) ID of the node"

instance ToCapture (Capture "goalId" GoalId) where
  toCapture _ =
    DocCapture "goalId"
               "(integer) ID of the target goal formula"

instance ToCapture (Capture "rootId" RootId) where
  toCapture _ =
    DocCapture "rootId"
               "(integer) ID of the root node"

instance ToCapture (Capture "maxLevel" Int) where
  toCapture _ =
    DocCapture "maxLevel"
               "(integer) maximum number of levels of the proof to return"

instance ToCapture (Capture "gas" Int) where
  toCapture _ =
    DocCapture "gas"
               "(integer) how many iterations to use for proof attempt"

sampleNode16 :: ProofNode
sampleNode16 = ProofNode {_id = NodeId 16, formula = "(-> A1 A1)", kind = Rule "impIntro", role = "pred", children = Just [sampleNode14]}

sampleNode14 :: ProofNode
sampleNode14 = ProofNode {_id = NodeId 14, formula = "A1", kind = Leaf, role = "leaf", children = Nothing}

instance {-# OVERLAPPING #-} ToSample String where
  toSamples _ = samples ["(A1 -> (A2 -> A1))", "A4"]

instance ToSample NodeId where
  toSamples _ = samples [NodeId 42, NodeId 27]

instance ToSample GoalId where
  toSamples _ = samples [GoalId 42, GoalId 27]

instance ToSample RootId where
  toSamples _ = samples [RootId 42, RootId 27]

instance ToSample FormulaId where
  toSamples _ = samples [FormulaId 42, FormulaId 27]

instance ToSample ProofNode where
  toSamples _ = samples [sampleNode14, sampleNode16]

instance ToSample Text where
  toSamples _ = samples ["(A1 -> (A2 -> A1))", "A4"]

instance ToSample (ReqResult NodeId) where
  toSamples _ = samples [ReqResult (NodeId 76) (NodeId 83), ReqResult (NodeId 45) (NodeId 52)]

instance ToSample (ReqResult GoalId) where
  toSamples _ = samples [ReqResult (NodeId 76) (GoalId 83), ReqResult (NodeId 45) (GoalId 52)]

instance ToSample (ReqResult ProofNode) where
  toSamples _ = samples [ReqResult (NodeId 12) sampleNode14, ReqResult (NodeId 63) sampleNode16]

instance ToSample (ReqResult (ProofNode, ProofNode)) where
  toSamples _ = samples [ReqResult (NodeId 12) (sampleNode14, sampleNode16)]

instance ToSample () where
  toSamples _ = singleSample ()

userServer :: ServerT ProtectedUserAPI (App IO)
userServer _user = newProofS
     :<|> loadProofS
     :<|> deleteRootS
     :<|> revertProofS
     :<|> showRootsS
     :<|> impIntroS
     :<|> impElimS
     :<|> solveGoalS
     :<|> checkAuthS
     :<|> eliminatorsS
     :<|> hypothesesS
  where
    newProofS :: String -> App IO (Either String (ReqResult GoalId))
    newProofS = loadRoot
    loadProofS :: RootId -> Int -> App IO [ProofNode]
    loadProofS rootId maxLevel = proofTree maxLevel rootId
    deleteRootS :: RootId -> App IO Bool
    deleteRootS = deleteProof
    revertProofS :: NodeId -> App IO NodeId
    revertProofS = revertProof
    showRootsS :: App IO [(RootId, Text)]
    showRootsS = showRoots
    impIntroS :: GoalId -> App IO (ReqResult ProofNode)
    impIntroS = implicationIntroBackwardInteractive
    impElimS :: GoalId -> FormulaId -> App IO (ReqResult (ProofNode, ProofNode))
    impElimS = implicationElimBackwardInteractive
    solveGoalS :: Int -> GoalId -> App IO ()
    solveGoalS = fmap void . solveGoal
    checkAuthS = return True
    eliminatorsS goalId = bolt $ possibleEliminators goalId
    hypothesesS :: GoalId -> App IO [(FormulaId, Text)]
    hypothesesS = goalHypotheses

userAPI :: Proxy UserAPI
userAPI = Proxy

protectedUserAPI :: Proxy ProtectedUserAPI
protectedUserAPI = Proxy

apiJS :: Text
apiJS = jsForAPI protectedUserAPI vanillaJS


docsBS :: ByteString
docsBS = encodeUtf8
       . pack
       . markdown
       $ docsWithIntros [intro] userAPI
  where intro = DocIntro "Welcome" ["This is `gtp`'s webservice's API."]

type DocsAPI
  = ProtectedUserAPI
  :<|> "api.js" :> Raw
  :<|> Raw


fullAPI :: Proxy DocsAPI
fullAPI = Proxy


server :: ServerT DocsAPI (App IO)
server = userServer :<|> Tagged serveApiJS :<|> Tagged serveDocs
  where
    serveApiJS _ respond = respond $ responseBuilder ok200 [javascript] js
      where
        javascript = ("Content-Type", "text/javascript; charset=utf-8")
        js = encodeUtf8Builder apiJS
    serveDocs _ respond =
      respond $ responseLBS ok200 [plain] docsBS
      where
        plain = ("Content-Type", "text/plain; charset=utf-8")

nt :: Config -> App IO a -> Handler a
nt c x = liftIO $ runReaderT x c


app :: Config -> UserDB -> Application
app config userDB = myCors . serveWithContext fullAPI ctx
  $ hoistServerWithContext fullAPI (Proxy :: Proxy '[BasicAuthCheck User])
                           (nt config) server
  where
    ctx = checkBasicAuth userDB :. EmptyContext
    myCors = cors (const . Just $ corsPolicy)
    -- can't use simpleCors because we use authentication header
      where
        corsPolicy = simpleCorsResourcePolicy
                     { corsRequestHeaders = [ "Authorization", "Content-Type" ]
                     }

-- | Log level of gtp server
data LogLevel
  = None -- ^ No logging
  | Dev -- ^ Detailed logging
  | Prod -- ^ Minimal logging
  deriving (Eq, Read, Show)

gtpServe :: LogLevel -> Port -> App IO ()
-- | Serve 'UserAPI' under the given log level and port.
gtpServe logLevel port = do
  liftIO . putStrLn $ "> Serving gtp API at port " ++ show port
  config@Config{serverC = userDB} <- ask
  let logger =
        -- IDEA: maybe log using Network.Wai.Logger?
        case logLevel of
          None -> id
          Dev  -> logStdoutDev
          Prod -> logStdout
  liftIO . run port . logger $ app config userDB
