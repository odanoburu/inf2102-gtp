{-|
Module      : Graph.ATP
Description : Automatic graph theorem prover
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare functions attempting automatic proofs. All @solve*@ functions
are limited by an integer dubbed @gas@; each iteration uses one unit
of gas, and when gas is over the proof attempt fails. When one of
these functions receives another integer it is an identifier
specifying part of a proof: a goal or a root node. These are the
identifiers used by the graph backend (Neo4j).
-}

-- clonar goals, aplicar regras, e colocar rule nodes como competing

-- OPTIMIZE: use more cores (we can't solve competing goals
-- asynchronously easily, but we do it for sibling goals)
--- OPTIMIZE: use neo4j indices
-- OPTIMIZE: profile queries: how?
--- OPTIMIZE: because most of the time is spent in neo4j, profiling of
--- haskell code is a bit useless

{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE QuasiQuotes #-}
module Graph.ATP (ATPRes(..), solveGoal, solveFormula, solveRoot) where

import Graph.Bolt ( (=!), FormulaId, GoalId(..), NodeId(..), RootId(..)
                  , bolt, boltTransact, cloneNode, remainingGoals
                  , discardHypothesisCloseBranch, goalTypeCrown
                  , implicationElimBackward, loadFormula, maybeResult
                  , oneResult, possibleEliminators
                  , remainingGoalsByLeafOrGoal, remainingGoalsByRoot)
import Logic.MIMP (Formula)
import Graph.Lib (App, logMsg)

import Control.Monad (foldM, zipWithM)
import Control.Monad.IO.Class (liftIO)
import Database.Bolt ((=:), BoltActionT, props, queryP_)
import Data.Coerce (coerce)
import Data.Functor (($>))
import Data.List (unzip5)
import Data.String.Interpolate (i)
--import Debug.Trace (trace)


data Appl = Failed | New [GoalId] | Closed NodeId deriving (Show)
-- IDEA: Closed could include the competing goals that it has turned
-- stale, which could be used to fix semigroup instance

instance Semigroup Appl where
  Failed <> x = x
  (New gs) <> (New gs') = New $ gs <> gs'
  (New gs) <> _ = New gs -- wrong?
  (Closed _) <> New gs = New gs -- wrong?
  (Closed leafId) <> _ = Closed leafId

instance Monoid Appl where
  mempty = Failed


makeCompetition :: GoalId -> [NodeId] -> BoltActionT IO ()
makeCompetition goalId ruleIds@(_:_:_)
  = queryP_ "CALL custom.gtp.atp.makeCompetition($goalId, $ruleIds)" params
  where
    params = props ["goalId" =: goalId, "ruleIds" =: ruleIds]
makeCompetition _ _ = fail "Internal error: needs at least two competitors"


maybeCloseCompetition :: NodeId -> BoltActionT IO (Maybe NodeId)
-- |
maybeCloseCompetition leafId =
  maybeResult "maybeCloseCompetition" (=! "winnerId")
    "CALL custom.gtp.atp.maybeCloseCompetition($leafId) YIELD maybeWinnerId" params
  where
    params = props ["leafId" =: leafId]


implicationElimBackwardAutomatic :: GoalId -> FormulaId -> BoltActionT IO Appl
implicationElimBackwardAutomatic goalId elimId = do
  (etedId, isEtedLeaf, etorId, isEtorLeaf) <- oneResult "impElimBackAuto"
    (\r -> (r =! "etedGoalId", r =! "isEtedLeaf", r =! "etorGoalId", r =! "isEtorLeaf"))
    "CALL custom.gtp.atp.implicationElimBackward($goalId, $elimId) YIELD etedGoalId, isEtedLeaf, etorGoalId, isEtorLeaf" params
  case (isEtedLeaf, isEtorLeaf) of
    (True, True) -> return $ Closed etorId
    (True, False) -> return $ New [coerce etorId]
    (False, True) -> return $ New [etedId]
    _ -> return $ New [etedId, coerce etorId]
  where
    params = props ["goalId" =: goalId, "elimId" =: elimId]


allImplicationElimBackwardAutomatic :: GoalId -> BoltActionT IO Appl
allImplicationElimBackwardAutomatic goalId = do
  elimIds <- fmap fst <$> possibleEliminators goalId
  case elimIds of
    [] -> return Failed
    [elimId] -> implicationElimBackwardAutomatic goalId elimId
    (_:elimIds') -> do
      -- create clone goals for competitors
      goalIds' <- mapM (const $ coerce cloneNode goalId) elimIds'
      let goalIds = goalId : coerce goalIds'
      -- apply elimination
      results <- zipWithM implicationElimBackward goalIds elimIds
      let (ruleNodeIds, etedIds, _, etorIds, _) = unzip5 results
      _ <- makeCompetition (head etedIds) ruleNodeIds
      foldM go Failed $ zip etedIds etorIds
  where
    go (Closed leafId) _ = return $ Closed leafId
    go result (etedId, etorId) = do
      maybeEtedLeaf <- discardHypothesisCloseBranch etedId
      maybeEtorLeaf <- discardHypothesisCloseBranch etorId
      case (maybeEtedLeaf, maybeEtorLeaf) of
        (Just _, Just etorLeaf) -> return (Closed etorLeaf)
        (Just _, Nothing) -> return $ New [etorId] <> result
        (Nothing, Just _) -> return $ New [etedId] <> result
        _ -> return $ New [etedId, etorId] <> result


implicationIntroBackwardAutomatic :: GoalId -> BoltActionT IO Appl
implicationIntroBackwardAutomatic goalId = do
  result <- maybeResult "implicationIntroBackwardAutomatic"
                                   (\r -> (r =! "goalId", r =! "isLeaf"))
                                   "CALL custom.gtp.atp.implicationIntroBackward($goalId)"
                                   params
  
  case result of
    Just (newGoalId, isLeaf) ->
      return $ if isLeaf
      then Closed newGoalId
      else New [coerce newGoalId]
    Nothing -> return Failed
  where
    params = props ["goalId" =: goalId]


apply :: BoltActionT IO Appl -> BoltActionT IO Appl
apply a = do
  r <- a
  case r of
    Closed leafId -> maybeCloseCompetition leafId $> r
    _ -> return r


data JustReachable = No | YesIntro | YesElim FormulaId
  deriving Show


goalJustReachable :: GoalId -> BoltActionT IO JustReachable
-- OPTIMIZE: maybe don't check if its introReachable, since if it's
-- not elimReachable impIntro will be applied (on the other hand, if
-- it is introReachable, we wouldn't check for elimReachable…)
--- OPTION: instead of doing the reachability thing, simply apply
--- several rules in competition, if any reaches the goal then boom,
--- else delete the competition and continue only with impIntro; it
--- could be optimal to do something like this, but with the current
--- tools it would be wasteful and still not very modular (like this
--- approach)
goalJustReachable goalId = do
  result <- maybeResult "introReachable" (const ()) reachableByIntro params
  case result of
    -- check if there was a result or not
    Nothing -> do
      maybeEtorId <- maybeResult "elimReachable" (=! "etorId") reachableByElim params
      return $ maybe No YesElim maybeEtorId
    Just _ -> return YesIntro
  where
    reachableByIntro =
      "CALL custom.gtp.atp.goalReachable.byIntro($goalId)"
    reachableByElim =
      "CALL custom.gtp.atp.goalReachable.byElim($goalId) YIELD etorId"
    params = props ["goalId" =: goalId]


willCycleIfImpIntro :: GoalId -> BoltActionT IO Bool
willCycleIfImpIntro goalId = oneResult "willCycle" (=! "willCycle") willCycle params
  where
    willCycle =
      "CALL custom.gtp.atp.willCycleIf.impIntro YIELD willCycle"
    params = props ["goalId" =: goalId]


data SearchRes = OutOfGas -- ^ remaining goals
  | NoSolution [GoalId]
  | Done Int -- ^ remaining gas
  deriving Show


solveGoals :: Int -> [GoalId] -> BoltActionT IO SearchRes
solveGoals = go
  where
    go 0 _ = return OutOfGas
    go remainingGas [] = return $ Done remainingGas
    go limit nodeIds = do
      goalIds <- activeGoals nodeIds
      result <- mconcat <$> mapM applyRule goalIds
      case result of
        Failed -> do
          goals <- foldM collectNewGoals [] goalIds
          case goals of
            [] -> return $ Done limit
            _ -> return $ NoSolution goals
        Closed leafId -> do
          goals <- remainingGoalsByLeafOrGoal leafId
          case goals of
            [] -> return $ Done limit
            -- DOUBT: does this ever happen? I don't think so
            _ -> fail "Hmm…[2]"
        New newGoalIds -> go (limit-1) newGoalIds
    collectNewGoals [] goalId = remainingGoalsByLeafOrGoal $ coerce goalId
    collectNewGoals xs _ = return xs
    isActiveGoal nodeId = not.null <$> activeGoals [nodeId]
    activeGoals :: [GoalId] -> BoltActionT IO [GoalId]
    activeGoals nodeIds = oneResult "activeGoals" (=! "goalIds")
      "RETURN [nodeId IN $nodeIds WHERE custom.gtp.isGoal(nodeId) | nodeId] AS goalIds" $ props ["nodeIds" =: nodeIds]
    applyRule :: GoalId -> BoltActionT IO Appl
    applyRule goalId = do
      goalActive <- isActiveGoal goalId
      if goalActive then do
        justReachable <- goalJustReachable goalId
        case justReachable of
          No -> do
            dontImpIntro <- willCycleIfImpIntro goalId
            if dontImpIntro
              then apply $ allImplicationElimBackwardAutomatic goalId
              else do
              result <- apply $ implicationIntroBackwardAutomatic goalId
              case result of
                Failed -> apply $ allImplicationElimBackwardAutomatic goalId
                _ -> return result
          YesIntro -> apply $ implicationIntroBackwardAutomatic goalId
          YesElim etorId -> apply $ implicationElimBackwardAutomatic goalId etorId
        else return Failed


solveRootGoal :: Int -> GoalId -> BoltActionT IO SearchRes
solveRootGoal gas rootGoalId
  =
  -- OPTION: could also get root node here (or take it as argument),
  -- and check if its actually solved at the end
  isGoalRoot
  *> rootToAtom rootGoalId
  >>= solveGoals gas
  where
    rootToAtom goalId = do
      -- apply implication introduction to root node repeatedly
      result <- implicationIntroBackwardAutomatic goalId
      case result of
        New [newGoalId] -> rootToAtom newGoalId
        Closed leafId -> return [coerce leafId]
        Failed -> return [coerce goalId]
        _ -> fail "solveRootGoal: shouldn't return more than one goal"
    isGoalRoot = do
      result <- maybeResult "rootGoal" id connectedToRoot params
      case result of
        Nothing -> fail "goalId provided is not a goal connected to a root"
        Just _ -> return ()
      where
        connectedToRoot =
          [i|MATCH (goal:Meta {name: "goal"})
WHERE id(goal) = $goalId
WITH goal
MATCH (:Rule {name: "root"})-[:DerivedBy {goal: true}]->(goal)
RETURN true|]
        params = props ["goalId" =: rootGoalId]


-- | Data type for result of automatic proof attempt
data ATPRes
  = Solved Int -- ^ Proof was found, there is remaining gas
  | NeedsMoreGas -- ^ Could not find proof with available gas
  | Unsolved [GoalId] -- ^ Could not find proof: there are remaining
                      -- goals
  deriving (Eq, Show)


reportSearchResult :: SearchRes -> [GoalId] -> ATPRes
reportSearchResult OutOfGas _ = NeedsMoreGas
reportSearchResult (NoSolution goalIds) _ = Unsolved goalIds
reportSearchResult (Done remainingGas) [] = Solved remainingGas
reportSearchResult (Done _) goals = Unsolved goals


solveRoot :: Int -> RootId -> App IO ATPRes
-- | @'solveRoot' gas rootId@: Solve all goals connected to root node
-- with id @rootId@ using at most @gas@
solveRoot gas rootId = boltTransact $ do
  goalIds <- remainingGoalsByRoot rootId
  case goalIds of
    [] -> return $ Solved gas
    [goalId] -> solveGoal_ gas goalId
    _ -> solveRoot' goalIds
  where
    solveRoot' goalIds = do
      result <- solveGoals gas goalIds
      reportSearchResult result <$> remainingGoalsByRoot rootId

solveGoal_ :: Int -> GoalId -> BoltActionT IO ATPRes
solveGoal_ gas goalId
  = do
  (crownId, isRootGoal) <- goalTypeCrown goalId
  result <- getSearchRes isRootGoal
  reportSearchResult result <$> remainingGoals crownId
  where
    getSearchRes True = solveRootGoal gas goalId
    getSearchRes False = solveGoals gas [goalId]


solveGoal :: Int -> GoalId -> App IO ATPRes
-- | @'solveGoal' gas goalId@: Solve goal with id @goalId@ using at
-- most @gas@
solveGoal gas goalId = boltTransact $ solveGoal_ gas goalId


solveFormula :: Int -> Formula -> App IO ATPRes
-- | Attempt proof of formula
solveFormula gas rootFormula = bolt $ do
      (rootId, rootGoalId) <- loadFormula rootFormula
      liftIO $ logMsg "> Sucessfully loaded formula"
      searchRes <- solveRootGoal gas rootGoalId
      remGoals <- remainingGoalsByRoot rootId
      return $ reportSearchResult searchRes remGoals

