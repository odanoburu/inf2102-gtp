{-|
Module      : Graph.Reader
Description : Functions for parsing and generating graph formulas
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Parse graphs and generate formulas about them.
-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StrictData #-}

module Graph.Reader (Graph(..), PathStop, parseGraph, economicalHamiltonian) where

import Logic.CP (Formula'(..))
import Logic.Embed (HasBottom(..), Next(..), true)

--import Debug.Trace (trace)
--import Text.Megaparsec.Debug (dbg)
import Control.Monad (void)
import Data.Bifunctor (bimap)
import Data.Functor (($>))
import Data.List (tails)
import qualified Data.Set as S
import Data.Void (Void)
import Prelude hiding (head, max)
import Text.Megaparsec hiding (some, token, label)
import Text.Megaparsec.Char (eol, space)
import Text.Megaparsec.Char.Lexer (decimal)


type Parser = Parsec Void String

-- | datatype for graphs in adjacency list format
newtype Graph a = Graph [(a, [a])] deriving (Eq, Show)

graph :: Parser a -> Parser (Graph a)
graph p = Graph <$> (space *> go <* eof)
  where
    go = do
      head <- nodeId
      nodeEdges <- (head,) <$> manyTill nodeId (eol *> space <|> eof)
      moreNodeEdges <- eof $> [] <|> go
      return $ nodeEdges:moreNodeEdges
        where
          nodeId = p <* spaces
          spaces = void $ takeWhileP Nothing (== ' ')


parseGraph :: String -> Either String (Graph Int)
-- | parse graph from a string. Graph is specified as an adjacency
-- list where each line is composed by a node id (the head), and any
-- number of other node ids (there is an edge from the head to these)
parseGraph input
  = bimap errorBundlePretty id
  $ parse (graph decimal) "" input


---
-- generate hamiltonian path problem formula

-- we define a special datatype so we can customize its typeclass
-- instances
--- PS n a means that atom a is visited at step n
-- | In the logical encoding of the Hamiltonian path problem we create
-- propositions indexed by an integer (the step in the path) and by
-- the original data representing the vertex being visited
data PathStop a = PS Int a
  deriving (Eq, Ord)

instance HasBottom a => HasBottom (PathStop a) where
  bot = PS bot bot

instance Next a => Next (PathStop a) where
  -- this instance doesn't make much sense, but oh well… we can know
  -- that PS n a has no meaning if a does not belong to the original
  -- graph
  next (PS _ a) = PS bot (next a)

instance Show a => Show (PathStop a) where
  -- FIXME: hack
  show (PS n a) = concat [show n, "at", show a]

---- deprecated in favor of economical hamiltonian, but keeping it
---- here since it is easier to understand
-- hamiltonianFormula :: (HasBottom a, Ord a) => Graph a -> Formula' (PathStop a)
-- hamiltonianFormula (Graph []) = undefined
-- hamiltonianFormula (Graph gr)
--   = (/\)[ anyVertexAnytime, noVerticesTwice
--         , eachStepOneVertex, noStepTwoVertices
--         , noVisitNextNonAdjacent ] (fmap $ uncurry PS)
--   where
--     anyVertexAnytime = (/\)vs $ (\/)steps. mayVisitAnytime
--       where
--         mayVisitAnytime = at
--     noVerticesTwice = (/\)vs $ (/\)(pairs steps). noVertexTwoSteps
--       where
--         noVertexTwoSteps v (i, j) = Not $ And (v `at` i) (v `at` j)
--     eachStepOneVertex = (/\)steps $ (\/)vs. oneStepOneVertex
--       where
--         oneStepOneVertex = flip at
--     noStepTwoVertices = (/\)(pairs vs) $ (/\)steps. noTwoVerticesSameStep
--       where
--         noTwoVerticesSameStep (v,w) i = Not $ And (v `at` i) (w `at` i)
--     noVisitNextNonAdjacent =
--       (/\)nonexistentEdges $ (/\)(init steps). noEdgeNoVisitNext
--       where
--         noEdgeNoVisitNext (v,w) i = Impl (v `at` i) (Not (w `at` (i+1)))
--         nonexistentEdges = filter (`S.notMember` es) $ pairs vs
--     vs = fmap fst gr
--     -- OPTIMIZE: if we assume some sort of order we can use
--     -- fromDistinctAscList
--     es = S.fromList $ concatMap (\(v, ts) -> fmap (v,) ts) gr
--     [] \/ _ = false
--     is \/ f = foldr1 Or $ fmap f is
--     steps = [1..length vs]


pairs :: [a] -> [(a, a)]
pairs l = [(x,y) | (x:ys) <- tails l, y <- ys] -- unique pairs if list
                                               -- is unique
at :: a -> b -> Formula' (b, a)
-- v is visited at step t
v `at` t = Atom (t, v)

(/\) :: (HasBottom b) => [a] -> (a -> Formula' b) -> Formula' b
[] /\ _ = true
is /\ f = foldr1 And $ fmap f is

(-->) :: (HasBottom b) => [a] -> (a -> Formula' b) -> Formula' b
[] --> _ = true
is --> f = foldr1 Impl $ fmap f is


economicalHamiltonian :: (HasBottom a, Next a, Ord a) => Graph a -> Formula' (PathStop a)
-- | generate classical propositional formula stating that the input
-- graph does not have a hamiltonian path. See the paper
-- \'Exponentially Huge Natural Deduction proofs are Redundant:
-- Preliminary results on M⊃\', by Edward Hermann Haeusler (available
-- at <https://arxiv.org/abs/2004.10659>)
economicalHamiltonian (Graph []) = undefined
economicalHamiltonian (Graph gr) =
  (-->)[ noStayPut
       , noImpossibleTransition
       , anyVertexLeadsToBottom
       ,  (/\)steps $ \t -> Atom (t, anyVertex t)
       , bottom
       ] (fmap $ uncurry PS)
  where
    noStayPut = (/\)steps $ (/\)vs. go
      where
        go t v = Impl (v `at` t) (Impl (v `at` (t+1)) bottom)
    noImpossibleTransition = (/\)steps $ (/\)nonEdges. go
      where
        go t (v, w) = Impl (v `at` t) (Impl (w `at` (t+1)) bottom)
        possibleEdges = pairs vs
        nonEdges = filter (`elem` es) possibleEdges
    anyVertexLeadsToBottom = (/\)steps $ \t -> (-->)(vs ++ [anyVertex t]) (go t)
      where
        go t v = Impl (v `at` t) bottom
    -- Atom (anyVertex t, t) encodes the formula \/v ∋ vertex. visits v at time t
    anyVertex t = ntimes next t max
    max = maximum vs
    vs = fmap fst gr
    es = S.fromList $ concatMap (\(v, ts) -> fmap (v,) ts) gr
    steps = [1..length vs]
    bottom = Atom bot


ntimes :: (a -> a) -> Int -> a -> a
ntimes f = go
  where
    go 0 a = a
    go n a = go (n-1) (f a)
