{-|
Module      : Graph.Auth
Description : Authentication for the gtp server
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Define authentication for the gtp server. Declares types for users,
passwords and user databases, plus functions for authenticating users.
-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Graph.Auth (User(..), Users(..), UserDB, Username, Password,
                   createUserDB, checkBasicAuth) where

import Data.Aeson (ToJSON, FromJSON)
import Data.Default (Default(..))
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Map (Map)
import qualified Data.Map.Strict as M
import GHC.Generics (Generic)
import Servant ( (:>), BasicAuth, BasicAuthCheck(..), BasicAuthResult(..)
               , Proxy(..), basicAuthUsername
               , basicAuthPassword)
import Servant.Foreign ( Arg(..), HasForeign(..), HasForeignType(..)
                       , HeaderArg(..), PathSegment(..)
                       , Req(..), foreignFor, typeFor)


-- orphan instance for basic authentication in generated JS code
instance (HasForeign lang ftype api,  HasForeignType lang ftype Text) => HasForeign lang ftype (BasicAuth a b :> api) where
  type Foreign ftype (BasicAuth a b :> api) = Foreign ftype api
  foreignFor lang proxy1 Proxy subR = foreignFor lang proxy1 (Proxy :: Proxy api) req
    where
      req = subR {_reqHeaders = HeaderArg arg : _reqHeaders subR}
      arg =
        Arg
          { _argName = PathSegment "Authorization"
          , _argType =
              typeFor lang (Proxy :: Proxy ftype) (Proxy :: Proxy Text)
          }


type Username = Text
type Password = Text

-- | datatype for gtp users
data User = User
  { user :: Username
  , pass :: Password
  } deriving (Eq, Generic, Show, ToJSON, FromJSON)

-- could be a postgres connection, a file, anything.
-- | datatype for user database (currently kept in memory since we
-- don't expect large use bases)
type UserDB = Map Username User

newtype Users = Users [User]
  deriving (Generic, ToJSON, FromJSON)

instance Default Users where
  def = Users [User "Hermann" "LogicIsLife", User "Prawitz" "NaturallyDeducting"]

createUserDB :: Users -> UserDB
-- | create user database from list of users
createUserDB (Users users) = M.fromList [ (user u, u) | u <- users ]


-- provided we are given a user database, we can supply
-- a function that checks the basic auth credentials
-- against our database.
checkBasicAuth :: UserDB -> BasicAuthCheck User
-- NOTE: this only allows static users, since we have no way of
-- supplying new user dynamically, plus we're not even using a real
-- database for now
-- | checks user authentication against user database using Basic
-- Authentication
-- (<https://en.wikipedia.org/wiki/Basic_access_authentication>)
checkBasicAuth db = BasicAuthCheck $ \basicAuthData ->
  let username = decodeUtf8 (basicAuthUsername basicAuthData)
      password = decodeUtf8 (basicAuthPassword basicAuthData)
  in
  case M.lookup username db of
    Nothing -> return NoSuchUser
    Just u  -> if pass u == password
               then return (Authorized u)
               else return BadPassword

