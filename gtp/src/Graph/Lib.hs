{-|
Module      : Graph.Lib
Description : Library for graph proofs
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Library module for working with proofs represented as graphs. Mostly
export useful functions from other modules.
-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Graph.Lib (
  App,
  Config(..),
  GtpConfig(..),
  ServerCfg,
  getDefaultConfigDir,
  logMsg,
  readConfig,
  runApp,
  ) where

import Graph.Auth (UserDB, createUserDB)


import Control.Monad.Reader (ReaderT, liftIO, runReaderT)
import Data.Aeson (FromJSON, ToJSON, eitherDecodeFileStrict', encodeFile)
import Data.Default (Default(..))
import Data.Maybe (fromMaybe)
import Database.Bolt (BoltCfg(..))
import GHC.Generics (Generic)
import System.Directory ( XdgDirectory(XdgConfig), canonicalizePath, createDirectoryIfMissing, doesFileExist, getXdgDirectory)
import System.FilePath ((</>), takeDirectory)
import System.IO (hPutStrLn, stderr)


logMsg, putErrLn :: String -> IO ()
putErrLn = hPutStrLn stderr
-- | Log message to standard error 
logMsg = putErrLn


deriving instance Generic BoltCfg
deriving instance ToJSON BoltCfg
deriving instance FromJSON BoltCfg

-- | Server configuration. Currently only includes the user database.
type ServerCfg = UserDB

-- | gtp configuration is the configuration of its components. See
-- 'readConfig'.
data Config = Config {
  boltC :: BoltCfg, -- ^ Configuration for the BOLT protocol used to
                    -- connect with Neo4j, place in /bolt.cfg/
                    -- file.
  serverC :: ServerCfg, -- ^ Configuration for gtp server. Placed in
                        -- /users.db/ since its (for now) only a user
                        -- database.
  gtp :: GtpConfig -- ^ gtp configuration, placed at /gtp.cfg/.
  }

-- | Data type for gtp configuration
data GtpConfig = GtpConfig {neoCommand :: Maybe String}
  deriving (Default, FromJSON, Generic, ToJSON)

-- | Data type for the whole application, using the ReaderT
-- pattern. This makes the application configuration available to all
-- functions in App
type App = ReaderT Config

getDefaultConfigDir :: IO FilePath
-- | Return gtp configuration directory. (gtp configuration is placed
-- by default in the directory specified by the freedesktop project.)
getDefaultConfigDir = getXdgDirectory XdgConfig "gtp"

readConfig :: FilePath -> IO Config
-- | Read gtp configuration.
-- If there are no configuration files available when gtp starts, the
-- program will create default configurations, which the administrator
-- can then edit.
readConfig cfgDir = Config
    <$> readCfgFile (cfgDir </> boltCfgFileName)
    <*> (createUserDB <$> readCfgFile (cfgDir </> serverUsersFileName))
    <*> readCfgFile (cfgDir </> gtpCfgFileName)
  where
    boltCfgFileName = "bolt.cfg"
    serverUsersFileName = "users.db"
    gtpCfgFileName = "gtp.cfg"
    readCfgFile :: forall a. (Default a, FromJSON a, ToJSON a) => FilePath -> IO a
    readCfgFile cfgFile = do
      cfgFileExists <- liftIO $ doesFileExist cfgFile
      if cfgFileExists
        then do
        eitherCfg <- liftIO $ eitherDecodeFileStrict' cfgFile
        case eitherCfg of
          Right cfg -> return cfg
          Left errorMsg
            -> error (unlines ["Failed to read configuration file at " ++ cfgFile
                              , "with error message:"
                              , errorMsg])
        else liftIO $ writeDefaultCfg
             *> error (unlines ["No configuration file found at " ++ cfgFile
                               , "Default configuration has been written to help editing"])
        where
          writeDefaultCfg = do
            createDirectoryIfMissing True $ takeDirectory cfgFile
            cfgFile `encodeFile` (def :: a)


runApp :: Maybe FilePath -> App IO a -> IO a
-- | Read the gtp configuration and then run the 'App' function with
-- it
runApp mConfigDir' app = do
  let mConfigDir = canonicalizePath <$> mConfigDir'
  configDir <- fromMaybe getDefaultConfigDir mConfigDir
  putErrLn $ "> Reading configuration from " ++ configDir
  config <- readConfig configDir
  runReaderT app config
